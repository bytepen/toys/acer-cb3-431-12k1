# Amazon Workspaces Client

## Summary

One of the biggest reasons I purchased this Acer N16P1 was to use the `Amazon Workspaces Client` for Android on something with a larger screen than my phone and a real keyboard. I have to say that the experience here is amazing coming from someone who has been trying to get by with Amazon's Linux Client. You can copy/paste in and out of the VM, keybindings don't get stuck, and when you're in full screen mode, you can't even tell it's not running on your local system. I'm thoroughly impressed with this setup, and it will lead me to using Amazon Workspaces a lot more in the future.

## Installation

The `Amazon Workspaces Client` can be installed just like any other Android app from the Google Play Store, which comes standard on the Acer N16P1.
