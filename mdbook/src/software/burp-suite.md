# Burp Suite

## Summary

Burp Suite is a network proxy allowing you to snoop on network requests in real time. It's a pretty neat tool, but might not be a great choice for the Acer N16P1 since it does like excess RAM and this chromebook only has 4GB. That said, I'm going to try it out and see if it works alright.

## Installation

The installation is the same as any other Burp Suite installation. You can download the most recent Community Edition [here](https://portswigger.net/burp/releases/professional-community-2020-5?requestededition=community) and then transfer it to the Linux subsystem by opening the `Files` app, locating it, and copying it to the `Linux files` area. From there, you just need to run the following commands inside the `Terminal` app:

```
chmod +x burpsuite*.sh
sudo ./burpsuite*.sh
```

Follow the instructions to install it as normal and you should be good to go. You can launch it just like any other application by pressing the `Search` button and looking for `Burp Suite Community Edition`.

One weird note about this setup is that due to the IP Addresses scheme noted within the [Connection Forwarder](/software/connection-forwarder.md) page, you will need to use [Connection Forwarder](/software/connection-forwarder.md) to access Burp Suite from another device on your network.
