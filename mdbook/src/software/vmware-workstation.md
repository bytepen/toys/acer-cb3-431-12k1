# VMWare Workstation

## Summary

VMWare Workstation is typically used to install Virtual Machines on computers. I'm under no dilusion that this Acer N16P1 would work well as a VM Host, however, it would be pretty awesome to be able to install VMWare Workstation on this device so that I could use it to access Virtual Machines on other device running VMWare Workspaces.

## Installation Notes

I had to first install the [Linux (Beta)](/software/linux-beta.md), which was super easy on this device and is detailed on that page.

From there, we launch a terminal app that drops us into a shell. The VMWare Workstation app is downloaded as a `.bundle` file and can be found [here](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html). Visit that URL in Chrome on the Acer and download it.

Once downloaded on your chromebook, open the `Files` app, locate the `.bundle` and move it to `Linux files` in the left column. If you open up the terminal and `ls` your home directory, you should see this file.

I had a bit of difficulty getting it to actually install properly as every time I tried to install it, it would fail to launch with `Segmentation fault` messages. It appears these issues are due to incorrect file permissions set while installing the program.

If you would like to make sure you are starting from scratch, you can uninstall it with the following command:

```
sudo vmware-installer -u vmware-workstation
```

I also had several messages while trying to run it talking about `libaio1` and `libpcsclite1`. I figured it wouldn't hurt to install those with the following command:

```
apt install libaio1 libpcsclite1
```

## Proper Installation

Install VMware Workstation with the following command.

```
sudo ./VMware*.bundle
```

You should be able to run it with `vmware`, however, this returns the following error:

```
/usr/bin/vmware: line 105:  <PID> Segmentation fault (core dumped) "$BINDIR"/vmware-modconfig --appname="VMware Workstation" --icon="vmware-workstation"
```

Looking at the `/usr/bin/vmware` file, we can see it is a script. After digging around on my Manjaro box, I found that the workaround employed there was to remove the problematic line and just pass it along. This can be done with the following command:

```
sudo sed -ibak 's/^if.*vmware-modconfig --appname.*&&$/if true ||/' /usr/bin/vmware
```

This will replace the problematic line back backup the original file to `/usr/bin/vmware.bak`. Running `vmware` now should result in a functional VMware Workstation installation. You should also be able to launch it like a normal program called `VMware Workstation`.
