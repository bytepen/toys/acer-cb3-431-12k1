# Linux (Beta)

## Summary

Linux (Beta) refers to the ability to install Linux applications on your Chromebook. Some chromebooks have a lot of hoops you have to jump through to enable this feature, but the Acer N16P1 couldn't have made this easier. You just need to go into `Settings` => `Linux (Beta)` and enable it. From there, you can launch `Terminal` just like any other application and you'll be dropped into the shell. The weird thing about this setup is that this is a virtual machine and because Chrome OS emphasises security, it isn't assigned an IP address on your local network. Instead, it is given an IP address in a segmented local network space that will be similar to `100.115.92.X`. If you're planning on using this for anything that you want to be accessed from outside of your local chromebook, you will need to forward some ports with a tool like [Connection Forwarder](/software/connection-forwarder.md).
