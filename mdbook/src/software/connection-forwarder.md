# Connection Forwarder

## Summary

[Connection Forwarder](https://chrome.google.com/webstore/detail/connection-forwarder/ahaijnonphgkgnkbklchdhclailflinn?hl=en-US) is a Chrome App that can be used to, as the title says, forward connections. It's pretty straight forward to use, you just launch it, click `Add Rule` and set up the source and destination port. One gotcha with this, is that it doesn't appear to list the IP address of the Linux subsystem as a useable destination IP Address for some reason, however, you can add it manually by first launching `Terminal` and running `ip a` to determine the IP Address (should be an interface like eth0@if6), then specifying `Custom` for the `Destination` Address and entering that IP in the `Custom Address` field. I usually leave the `Source Address` as `0.0.0.0`, but the `wlan0` option is probably always what you want. If it says a port is being used already, you may need to find a new port.

A rule allowing `Burp Suite` installed on the `Linux Subsystem` to be used by another device on your network might look something like this:

Protocol: `TCP`  
Source Address: `0.0.0.0`  
Source Port: `8080`  
Destination Address: `Custom`  
Custom Address: `100.115.92.182`
Destination Port: `8080`
