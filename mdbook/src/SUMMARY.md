# Summary

- [Acer N16P1](./index.md)
- [Software](./software/index.md)
  - [Amazon Workspaces Client](./software/amazon-workspaces-client.md)
  - [Burp Suite](./software/burp-suite.md)
  - [Connection Forwarder](./software/connection-forwarder.md)
  - [Linux (Beta)](./software/linux-beta.md)
  - [VMWare Workstation](./software/vmware-workstation.md)
- [Configuration](./configuration/index.md)
  - [More Storage](./configuration/more-storage.md)
