# More Storage

One of the "worst" things about this device is that it only has 32 GB of storage to work with. This is mostly fine since I'm not trying to do anything TOO crazy directly on this device, but I wanted the option to have more storage, so I put a 64 GB Sandisk Cruzer into one of the USB slots. I then formatted it as FAT32, opened `Files`, righted clicked it and selected `Share with Linux`. This allowed me to access it at `/mnt/chromeos/removable/USB`.

This isn't a GREAT solution, but it should work well enough for storing files that don't update SUPER often and that are on the larger side. If this proves to work out alright, there are much larger USB thumb drives for cheap, such as the [256 GB SanDisk Ultra Fit](https://www.amazon.com/SanDisk-256GB-Ultra-Flash-Drive/dp/B07857Y17V) for ~$35. I may pick up one of those eventually.
